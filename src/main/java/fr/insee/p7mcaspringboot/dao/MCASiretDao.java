package fr.insee.p7mcaspringboot.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.insee.p7mcaspringboot.model.MCASiret;

@Repository
public interface MCASiretDao extends JpaRepository<MCASiret, String>,MCASiretDaoPlus { 
	
	@Query("FROM MCASiret WHERE siret=:siret ")
	Set<MCASiret> findBySiret(@Param(value="siret") String siret);
	
	@Query("FROM MCASiret WHERE siren=:siren ")
	Set<MCASiret> findBySiren(@Param(value="siren") String siren);
	
	
	@Query("FROM MCASiret WHERE"
			+ " mot1||mot2||mot3||mot4||mot5||mot6||mot7 LIKE %:mot% "
			+ " AND dep_etab=:dep"
			+ " AND type_unite=:type")
	Set<MCASiret> findByMots(@Param(value="mot") String mot,
			@Param(value="dep") String dep,
			@Param(value="type") String type);
}
