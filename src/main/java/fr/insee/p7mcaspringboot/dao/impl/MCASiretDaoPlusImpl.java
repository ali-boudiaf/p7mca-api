package fr.insee.p7mcaspringboot.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import fr.insee.p7mcaspringboot.dao.MCASiretDaoPlus;
import fr.insee.p7mcaspringboot.model.MCASiret;

public class MCASiretDaoPlusImpl implements MCASiretDaoPlus {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Set<MCASiret> findByAdresse(String depEtab, String comEtab, String dermot, String adrVoieType,
			String adrVoieNum, String type) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<MCASiret> query = cb.createQuery(MCASiret.class);
		Root<MCASiret> user = query.from(MCASiret.class);

		Path<String> depEtabPath = user.get("depEtab");
		Path<String> typePath = user.get("typeUnite");
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(depEtabPath, depEtab));
		predicates.add(cb.equal(typePath, type));
		if (comEtab != null) {
			Path<String> comEtabPath = user.get("comEtab");
			predicates.add(cb.equal(comEtabPath, comEtab));
		}
		if (dermot != null) {
			Path<String> dermotPath = user.get("dermot");
			predicates.add(cb.equal(dermotPath, dermot));
		}
		if (adrVoieType != null) {
			Path<String> adrVoieTypePath = user.get("adrEtVoieType");
			predicates.add(cb.equal(adrVoieTypePath, adrVoieType));
		}
		if (adrVoieNum != null) {
			Path<String> adrVoieNumPath = user.get("adrEtVoieNum");
			predicates.add(cb.equal(adrVoieNumPath, adrVoieNum));
		}
		query.select(user).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		return entityManager.createQuery(query).getResultStream().collect(Collectors.toSet());
	}

}
