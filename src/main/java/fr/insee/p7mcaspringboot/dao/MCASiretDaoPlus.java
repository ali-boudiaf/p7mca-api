package fr.insee.p7mcaspringboot.dao;

import java.util.Set;

import fr.insee.p7mcaspringboot.model.MCASiret;

public interface MCASiretDaoPlus {
	Set<MCASiret> findByAdresse(String depEtab, String comEtab, String dermot, String adrVoieType, String adrVoieNum,
			String type);
}
