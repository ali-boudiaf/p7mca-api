package fr.insee.p7mcaspringboot.dao;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.insee.p7mcaspringboot.model.MCAMot;
import fr.insee.p7mcaspringboot.model.pk.MCAMotPK;

@Repository
public interface MCAMotDao extends JpaRepository<MCAMot, MCAMotPK> { 
	 
	@Query("FROM MCAMot WHERE ers=:mot and type_unite=:type and dep_etab=:dep")
	Collection<MCAMot> findByMot(@Param(value="mot") String mot ,@Param(value="type") String type,@Param(value="dep") String dep);
}
