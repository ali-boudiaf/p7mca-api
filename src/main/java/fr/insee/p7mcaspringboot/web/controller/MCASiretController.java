package fr.insee.p7mcaspringboot.web.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.p7mcaspringboot.model.MCASiret;
import fr.insee.p7mcaspringboot.service.MCAMotService;
import fr.insee.p7mcaspringboot.service.MCASiretService;
import fr.insee.p7mcaspringboot.utils.ElementAdresse;
import fr.insee.p7mcaspringboot.utils.NormalisationAdresse;
import fr.insee.p7mcaspringboot.utils.Notation;
import fr.insee.p7mcaspringboot.utils.NoteAdresse;
import fr.insee.p7mcaspringboot.utils.NoteNom;
import fr.insee.p7mcaspringboot.utils.TrancheEffectif;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("API pour les opérations CRUD sur les mca_siret")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class MCASiretController {

	@Inject
	MCASiretService mcaSiretService;
	@Inject
	MCAMotService mcaMotService;

	/*
	 * Récupérer un individus par son Id
	 */
	@ApiOperation(value = "Récupère un MCA Siret à partir du nummca")
	@GetMapping(value = "/mca-siret/nummca/{nummca}")
	public MCASiret recupererUnMcaSiretParNummca(@PathVariable String nummca) {
		return mcaSiretService.getDao().findById(nummca).orElse(null);
	}

	@ApiOperation(value = "Récupère un MCA Siret à partir du siret")
	@GetMapping(value = "/mca-siret/siret/{siret}")
	public Set<MCASiret> recupererDesMcaSiretParSiret(@PathVariable String siret) {
		return mcaSiretService.getDao().findBySiret(siret);
	}

	@ApiOperation(value = "Récupère un MCA Siret à partir du siren")
	@GetMapping(value = "/mca-siret/siren/{siren}")
	public Set<MCASiret> recupererDesMcaSiretParSiren(@PathVariable String siren) {
		return mcaSiretService.getDao().findBySiren(siren);
	}

	@ApiOperation(value = "rechercher des MCA Siret à partir de mots")
	@RequestMapping(method = RequestMethod.GET, value = "/mca-sirets/type/{type}/dep/{dep}/mots")
	public Set<MCASiret> recupererDesMcaSiretParMots(@RequestParam Map<String, String> mesMots,
			@PathVariable String type, @PathVariable String dep) {
		List<String> listeMot = mesMots.entrySet().stream().filter(e -> e.getKey().startsWith("mot"))
				.map(e -> e.getValue().toUpperCase()).collect(Collectors.toList());
		Set<MCASiret> mesMCASirets = new HashSet<>();
		for (String mot : listeMot) {
			mesMCASirets.addAll(mcaSiretService.getDao().findByMots(mot.toUpperCase(), dep, type.toUpperCase()));
		}
		int taille = mesMCASirets.size();
		Set<MCASiret> retour = new HashSet<>();
		retour.addAll(mesMCASirets.stream().sorted(
				(a, b) -> TrancheEffectif.get(b.getEff3112TrEt()).compareTo(TrancheEffectif.get(a.getEff3112TrEt())))

				.collect(Collectors.toList()).subList(0, 100 <= taille ? 100 : taille));
		return retour;
	}

	@ApiOperation(value = "rechercher des MCA Siret à partir de l'adresse")
	@RequestMapping(method = RequestMethod.GET, value = "/mca-sirets/type/{type}/dep/{dep}/adresse")
	public Set<MCASiret> recupererDesMcaSiretParAdresse(@RequestParam Map<String, String> mesTokenAdresse,
			@PathVariable String type, @PathVariable String dep) {
		String comEtab = mesTokenAdresse.containsKey("comEtab") ? mesTokenAdresse.get("comEtab") : null;
		String dermot = mesTokenAdresse.containsKey("adrVoieNom")
				? NormalisationAdresse.dernierMotVoie(mesTokenAdresse.get("adrVoieNom"))
				: null;
		String adrVoieType = mesTokenAdresse.containsKey("adrVoieType")
				? NormalisationAdresse.normaliserTypVoie(mesTokenAdresse.get("adrVoieType"))
				: null;
		String adrVoieNum = mesTokenAdresse.containsKey("adrVoieNum")
				? NormalisationAdresse.normaliserNumVoie(mesTokenAdresse.get("adrVoieNum"))
				: null;
		String adrVoieNom = mesTokenAdresse.containsKey("adrVoieNom") ? mesTokenAdresse.get("adrVoieNom") : null;
		String rs = mesTokenAdresse.containsKey("rs") ? mesTokenAdresse.get("rs") : null;
		List<String> listMotRs = Arrays.asList(rs.split(" ")).stream().map(s -> s.toUpperCase())
				.collect(Collectors.toList());
		Map<ElementAdresse, String> elementsAdresse = NormalisationAdresse.normaliserAdresse(dep, comEtab, adrVoieNum,
				adrVoieType, adrVoieNom);

		List<MCASiret> mesMCASiret = new ArrayList<>();
		mesMCASiret.addAll(mcaSiretService.getDao().findByAdresse(dep, comEtab, dermot, adrVoieType, adrVoieNum, type));
		for (MCASiret mcaSiret : mesMCASiret) {
			List<String> listMotEcho = new ArrayList<>();
			if (mcaSiret.getMot1() != null) {
				listMotEcho.add(mcaSiret.getMot1());
			}
			if (mcaSiret.getMot2() != null) {
				listMotEcho.add(mcaSiret.getMot2());
			}
			if (mcaSiret.getMot3() != null) {
				listMotEcho.add(mcaSiret.getMot3());
			}
			if (mcaSiret.getMot4() != null) {
				listMotEcho.add(mcaSiret.getMot4());
			}
			if (mcaSiret.getMot5() != null) {
				listMotEcho.add(mcaSiret.getMot5());
			}
			if (mcaSiret.getMot6() != null) {
				listMotEcho.add(mcaSiret.getMot6());
			}
			if (mcaSiret.getMot7() != null) {
				listMotEcho.add(mcaSiret.getMot7());
			}
			Map<ElementAdresse, String> mapAdresseEcho = new EnumMap<>(ElementAdresse.class);
			mapAdresseEcho.put(ElementAdresse.D, mcaSiret.getDepEtab());
			mapAdresseEcho.put(ElementAdresse.C, mcaSiret.getComEtab());
			mapAdresseEcho.put(ElementAdresse.NUMV, mcaSiret.getAdrEtVoieNum());
			mapAdresseEcho.put(ElementAdresse.TYPV,
					mcaSiret.getAdrEtVoieType() != null && mcaSiret.getAdrEtVoieType().length() > 0
							? mcaSiret.getAdrEtVoieType().substring(0, 1)
							: "");
			mapAdresseEcho.put(ElementAdresse.CADR, "");
			mapAdresseEcho.put(ElementAdresse.DERMOT, mcaSiret.getDermot());
			mcaSiret.setNoteAdresse(Notation.noterAdresse(elementsAdresse, mapAdresseEcho,
					mcaSiret.getAdrEtRivoli() != null && mcaSiret.getAdrEtRivoli().trim().length() != 0));
			mcaSiret.setNoteNom(Notation.noterNom(listMotRs, listMotEcho, mcaSiret.getOrigineErs()));
			NoteNom maNoteNom = new NoteNom(mcaSiret.getNoteNom());
			NoteAdresse maNoteAdresse = new NoteAdresse(mcaSiret.getNoteAdresse());
			mcaSiret.setNoteClasse(Notation.determinerClasse(maNoteNom, maNoteAdresse));
		}
		return mesMCASiret.stream().sorted().limit(100).collect(Collectors.toSet());
	}
}