package fr.insee.p7mcaspringboot.service;


import fr.insee.p7mcaspringboot.dao.MCAMotDao;

public interface MCAMotService {

	public MCAMotDao getDao();
}
