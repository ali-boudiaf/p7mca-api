package fr.insee.p7mcaspringboot.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fr.insee.p7mcaspringboot.dao.MCAMotDao;
import fr.insee.p7mcaspringboot.service.MCAMotService;

@Service
public class MCASiretServiceImpl implements MCAMotService {

	@Inject
	private MCAMotDao mcaMotDao;

	@Override
	public MCAMotDao getDao() {
		return mcaMotDao;
	}

}
