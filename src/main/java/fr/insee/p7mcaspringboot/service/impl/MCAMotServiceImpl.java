package fr.insee.p7mcaspringboot.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fr.insee.p7mcaspringboot.dao.MCASiretDao;
import fr.insee.p7mcaspringboot.service.MCASiretService;

@Service
public class MCAMotServiceImpl implements MCASiretService {

	@Inject
	private MCASiretDao mcaSiretDao;

	@Override
	public MCASiretDao getDao() {
		return mcaSiretDao;
	}

}
