package fr.insee.p7mcaspringboot.service;


import fr.insee.p7mcaspringboot.dao.MCASiretDao;

public interface MCASiretService {

	public MCASiretDao getDao();
}
