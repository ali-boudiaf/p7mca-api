package fr.insee.p7mcaspringboot.utils;

import java.util.HashMap;
import java.util.Map;

public enum TrancheEffectif {
	// l'ordre est important pour le compareTo
	NN("NN"), T00("00"), T01("01"), T02("02"), T03("03"), T11("11"), T12("12"), T21("21"), T22("22"), T31("31"), T32(
			"32"), T41("41"), T42("42"), T51("51"), T52("52"), T53("53");

	private String libelle;

	private TrancheEffectif(String st) {
		setLibelle(st);
	}

	public String getLibelle() {
		return libelle;
	}

	private void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	private static final Map<String, TrancheEffectif> LOOKUP = new HashMap<>();
	static {
		for (TrancheEffectif d : TrancheEffectif.values())
			LOOKUP.put(d.getLibelle(), d);
	}

	public static TrancheEffectif get(String libelle) {
		if (LOOKUP.containsKey(libelle)) {
			return LOOKUP.get(libelle);
		}
		return NN;
	}
}