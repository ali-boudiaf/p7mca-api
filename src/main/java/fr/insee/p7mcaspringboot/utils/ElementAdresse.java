package fr.insee.p7mcaspringboot.utils;

/**
 * Enumeration des éléments composants une adresse
 * <ul>
 * <li><code>D</code> : Département</li>
 * <li><code>C</code> : Commune</li>
 * <li><code>CADR</code> : Complément d'adresse</li>
 * <li><code>DERMOT</code> : Dernier mot du nom de la voie</li>
 * <li><code>TYPV</code> : Type de voie</li>
 * <li><code>NUMV</code> : Numéro dans la voie</li>
 * </ul>
 */
public enum ElementAdresse {

	D, C, CADR, DERMOT, TYPV, NUMV;

}
