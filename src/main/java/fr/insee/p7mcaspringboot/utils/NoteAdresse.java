package fr.insee.p7mcaspringboot.utils;

public class NoteAdresse implements INote {

	private int note;

	public NoteAdresse(int note) {
		this.note = note;
	}

	/**
	 * Evalue la classe d'une note d'adresse
	 * 
	 * @return La classe correspondant à la note :
	 *         <ul>
	 *         <li>10, 11 : Bon</li>
	 *         <li>20, 21, 22, 23 : Assez Bon</li>
	 *         <li>30, 31, 32, 35 : Mauvais</li>
	 *         </ul>
	 */
	public Classe classe() {
		switch (note) {
		case 10:
		case 11:
			return Classe.Bon;
		case 20:
		case 21:
		case 22:
			return Classe.AB;
		case 31:
		case 32:
		case 33:
		case 35:
		default:
			return Classe.Mauvais;
		}
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

}