package fr.insee.p7mcaspringboot.utils;

public class NoteNom implements INote {

	/**
	 *
	 * */
	public static final int SEUIL_BON = 800;
	public static final int SEUIL_AB = 200;

	private int note;

	public NoteNom(int note) {
		this.note = note;
	}

	/**
	 * Evalue la classe d'une note de raison sociale
	 * 
	 * @return La classe correspondant à la note :
	 *         <ul>
	 *         <li>800 < note : Bon</li>
	 *         <li>200 < note < 800 : Assez Bon</li>
	 *         <li>note < 200 : Mauvais</li>
	 *         </ul>
	 */
	public Classe classe() {
		if (note >= SEUIL_BON) {
			return Classe.Bon;
		} else if (note >= SEUIL_AB) {
			return Classe.AB;
		} else {
			return Classe.Mauvais;
		}
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

}