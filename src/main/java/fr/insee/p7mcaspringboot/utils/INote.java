package fr.insee.p7mcaspringboot.utils;

public interface INote {

	/**
	 * @return La classe d'une couple note
	 * @see fr.insee.p7.core.outils.chiffrement.mcarp.notation.Classe
	 */
	Classe classe();

	int getNote();

	void setNote(int note);
}