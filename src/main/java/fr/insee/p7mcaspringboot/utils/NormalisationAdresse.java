package fr.insee.p7mcaspringboot.utils;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class NormalisationAdresse {

	private static final List<String> MOTS_VOIE = Arrays.asList("BAT", "BP", "ZA", "ZAC", "ZI");

	public static String normaliserNumVoie(String unNumVoie) {
		if (unNumVoie == null || "".equals(unNumVoie.trim())) {
			return null;
		}
		int l = unNumVoie.length();
		switch (l) {
		case 1:
			return "000" + unNumVoie;
		case 2:
			return "00" + unNumVoie;
		case 3:
			return "0" + unNumVoie;
		default:
			return unNumVoie;
		}
	}

	public static String normaliserTypVoie(String unTypVoie) {
		if (unTypVoie == null || "".equals(unTypVoie.trim())) {
			return null;
		}
		return unTypVoie.substring(0, 1);
	}

	public static String dernierMotVoie(String unNomVoie) {
		if (unNomVoie == null || "".equals(unNomVoie.trim())) {
			return null;
		}

		/* 1. Mise en majuscule */
		unNomVoie = majuscule(unNomVoie);

		/* 2. Remplacement des caractère spéciaux par des espaces ou rien */
		unNomVoie = normaliserNomVoie(unNomVoie);

		/* 5. Repérage des mots spéciaux et retour du "dernier" mot */
		return dernierMot(unNomVoie);

	}

	public static String normaliserNomVoie(String unNomVoie) {
		unNomVoie = unNomVoie.replaceAll("\\.", "");
		unNomVoie = unNomVoie.replaceAll("\\&", "");
		unNomVoie = unNomVoie.replaceAll("\\W", " ");
		return unNomVoie;
	}

	private static String dernierMot(String unNomVoie) {
		String[] mots = unNomVoie.split(" ");
		for (int i = 0; i < mots.length; i++) {
			int indice = MOTS_VOIE.indexOf(mots[i]);
			if (indice != -1 && i != 0) {
				return (mots[i - 1]);
			}
		}
		if (mots.length != 0) {
			return (mots[mots.length - 1]);
		} else {
			return ("");
		}
	}

	public static String majuscule(String chaine) {

		if (chaine != null) {
			char[] chars = chaine.toCharArray();
			if (chars != null) {
				for (int i = 0; i < chars.length; i++) {
					char c = chars[i];
					switch (c) {
					case 192:
					case 193:
					case 194:
					case 196:
					case 224:
					case 225:
					case 226:
					case 228:
						chars[i] = 'A';
						break;

					case 199:
					case 231:
						chars[i] = 'C';
						break;

					case 200:
					case 201:
					case 202:
					case 203:
					case 232:
					case 233:
					case 234:
					case 235:
						chars[i] = 'E';
						break;

					case 206:
					case 207:
					case 238:
					case 239:
						chars[i] = 'I';
						break;

					case 212:
					case 214:
					case 244:
					case 246:
						chars[i] = 'O';
						break;

					case 217:
					case 219:
					case 220:
					case 249:
					case 251:
					case 252:
						chars[i] = 'U';
						break;

					case 195:
					case 197:
					case 198:
					case 204:
					case 205:
					case 208:
					case 209:
					case 210:
					case 211:
					case 213:
					case 215:
					case 216:
					case 218:
					case 221:
					case 222:
					case 223:
					case 227:
					case 229:
					case 230:
					case 236:
					case 237:
					case 240:
					case 241:
					case 242:
					case 243:
					case 245:
					case 247:
					case 248:
					case 250:
					default:
						chars[i] = Character.toUpperCase(c);
						break;
					}
				}

				return String.valueOf(chars);
			} else {
				return null;
			}
		} else {
			return "";
		}
	}

	public static Map<ElementAdresse, String> normaliserAdresse(String unCodeDepartement, String unCodeCommune,
			String unNumVoie, String unTypVoie, String unNomVoie) {

		Map<ElementAdresse, String> elements = new EnumMap<>(ElementAdresse.class);

		// Modification de la normalisation du département : comme pour le
		// transcodage pourSicore, on s'appuie sur la table MODALITES
		// et non sur un eliste géré dans un fichier départements.txt
		// String depCodeTranscode = null;
		// try {
		// depCodeTranscode =
		// EntrepriseFacade.getInstance().getEntrepriseService().getCommunesService()
		// .transcoderDepartementsAvantChiffre(unCodeDepartement);
		// } catch (GeographieException e) {
		// log.error("normaliserAdresse(unCodeDepartement=" + unCodeDepartement + ",
		// unCodeCommune=" + unCodeCommune
		// + ", unNumVoie=" + unNumVoie + ", unNomVoie=" + unNomVoie + ", unTypVoie=" +
		// unTypVoie + ")", e);
		// depCodeTranscode = unCodeDepartement;
		// }

		elements.put(ElementAdresse.D, unCodeDepartement);

		// Code avant modif
		// elements.put(ElementAdresse.D,normalisationAdresse.normaliserDepartement(unCodeDepartement));

		elements.put(ElementAdresse.C, unCodeCommune);
		elements.put(ElementAdresse.NUMV, normaliserNumVoie(unNumVoie));
		elements.put(ElementAdresse.TYPV, normaliserTypVoie(unTypVoie));
		elements.put(ElementAdresse.CADR, dernierMotVoie(unNomVoie));
		elements.put(ElementAdresse.DERMOT, dernierMotVoie(unNomVoie));

		return elements;
	}
}
