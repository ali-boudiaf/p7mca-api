package fr.insee.p7mcaspringboot.utils;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class Notation {

	private static final String SIGLE = "SG";
	private static final int NB_MOTS_MAX = 7;
	private static final int NB_LETTRE_MAX = 8;
	private static final int MATCH = 779;

	public static int noterNom(List<String> demande, List<String> voisin, String indnrs) {

		int note = 1000 * nbCaracteresIdentiques(demande, voisin) / nbCaracteresTotal(demande, voisin);
		if (note == 1000 && SIGLE.equals(indnrs)) {
			note = 800;
		}
		return note;
	}

	public static int noterAdresse(Map<ElementAdresse, String> demande, Map<ElementAdresse, String> voisin,
			boolean rivolise) {

		Map<ElementAdresse, Boolean> matches = new EnumMap<>(ElementAdresse.class);

		for (ElementAdresse element : ElementAdresse.values()) {
			matches.put(element, match(demande.get(element), voisin.get(element)));
		}

		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.C) && matches.get(ElementAdresse.DERMOT)
				&& matches.get(ElementAdresse.TYPV) && matches.get(ElementAdresse.NUMV)) {
			return 10;
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.C) && matches.get(ElementAdresse.DERMOT)
				&& matches.get(ElementAdresse.TYPV)) {
			if (rivolise) {
				return 21;
			} else {
				return 11;
			}
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.C) && matches.get(ElementAdresse.DERMOT)) {
			return 22;
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.C) && matches.get(ElementAdresse.CADR)) {
			return 23;
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.C)) {
			return 31;
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.DERMOT)) {
			return 32;
		}
		if (matches.get(ElementAdresse.D) && matches.get(ElementAdresse.CADR)) {
			return 33;
		}
		if (matches.get(ElementAdresse.D)) {
			return 35;
		}

		return 0;
	}

	public static int determinerClasse(NoteNom noteNom, NoteAdresse noteAdresse) {
		switch (noteNom.classe()) {
		case Bon:
			switch (noteAdresse.classe()) {
			case Bon:
				return 1;
			case AB:
				return 2;
			case Mauvais:
				return 3;
			}
		case AB:
			switch (noteAdresse.classe()) {
			case Bon:
				return 4;
			case AB:
				return 5;
			case Mauvais:
				return 6;
			}
		case Mauvais:
			switch (noteAdresse.classe()) {
			case Bon:
				return 7;
			case AB:
				return 8;
			case Mauvais:
				return 9;
			}
		}
		return 0;
	}

	public static int nbCaracteresTotal(List<String> demande, List<String> voisin) {
		int n1 = nbCaracteres(demande);
		int n2 = nbCaracteres(voisin);
		return Math.max(n1, n2);
	}

	public static int nbCaracteresIdentiques(List<String> demande, List<String> voisin) {
		List<String> matches = new ArrayList<>(NB_MOTS_MAX);
		List<String> motsUtilises = new ArrayList<>(NB_MOTS_MAX);

		for (String motD : demande) {
			for (String motV : voisin) {
				if (match(motD, motV) && !motsUtilises.contains(motV)) {
					matches.add(min(motD, motV));
					motsUtilises.add(motV);
				}
			}
		}
		return nbCaracteres(matches);
	}

	public static boolean match(String mot1, String mot2) {
		return (unimatch(mot1, mot2) >= MATCH);
	}

	public static int unimatch(String mot1, String mot2) {

		if (mot1 == null || mot2 == null) {
			return 0;
		}

		/* Le plus petit mot est la DONNEE. Le plus grand mot est la REFERENCE. */
		if (min(mot1, mot2).equals(mot2)) {
			String tmp = mot1;
			mot1 = mot2;
			mot2 = tmp;
		}

		/* Convertion des String en char[] */
		char[] donnee = mot1.toCharArray();
		char[] reference = mot2.toCharArray();

		/* Conservent les caractères communs au deux chaine dans l'ordre */
		List<Character> identifieDonnee = new ArrayList<>(NB_LETTRE_MAX);
		List<Character> identifieReference = new ArrayList<>(NB_LETTRE_MAX);

		/* (d) longueur du petit mot */
		/* (r) longueur du grand mot */
		/* (m) longueur du grand mot */
		/* (v) longueur du voisisnage */
		int d = donnee.length;
		int r = reference.length;
		int m = d;
		int v = m / 2 - 1;

		/* Mémorisent les caractère de la REFERENCE déja utilisés */
		boolean[] utilises = new boolean[r];
		for (int i = 0; i < r; i++) {
			utilises[i] = false;
		}

		/* Boucle sur les caractère de la DONNEE */
		for (int i = 0; i < d; i++) {
			char c = donnee[i];
			/*
			 * Correspondance parfaite entre le caractère de la DONNEE et celui de la
			 * REFERENCE
			 */
			if (donnee[i] == reference[i] && !utilises[i]) {
				identifieDonnee.add(c);
				utilises[i] = true;
			}
			/* Recherche d'une correspondance dans le voisinage de la REFERENCE */
			else {
				int indice = -1;
				/* Boucle sur les caractères du voisinage de la référence */
				for (int j = Math.max(0, i - v); j < Math.min(i + v, r); j++) {
					if (donnee[i] == reference[j] && !utilises[j]) {
						/*
						 * On a trouvé une correspondance. On ne la garde que si elle
						 */
						if (indice == -1 || Math.abs(j - i) < Math.abs(indice - i)) {
							indice = j;
						}
					}
				}
				if (indice != -1) {
					identifieDonnee.add(c);
					utilises[indice] = true;
				}
			}
		}

		for (int i = 0; i < r; i++) {
			if (utilises[i]) {
				identifieReference.add(reference[i]);
			}
		}

		int c = identifieDonnee.size();

		int t = 0;
		for (int i = 0; i < c; i++) {
			if (identifieDonnee.get(i) != identifieReference.get(i)) {
				t++;
			}
		}

		t = t / 2;

		int R = 0;
		if (c > 0) {
			R = (300 * c) / d + (300 * c) / r + (300 * (c - t)) / c;
		}

		return R;
	}

	public static int nbCaracteres(List<String> mots) {
		int nb = 0;
		for (String mot : mots) {
			nb += mot != null ? mot.length() : 0;
		}
		return nb;
	}

	public static String min(String a, String b) {
		if (a.length() <= b.length()) {
			return a;
		} else {
			return b;
		}
	}

}
