package fr.insee.p7mcaspringboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import fr.insee.p7mcaspringboot.model.pk.MCAMotPK;

@Entity
@Table(name = "mca_mots")
@IdClass(MCAMotPK.class)
public class MCAMot {

	@Id
	@Column(name = "dep_etab")
	private String depEtab;
	@Id
	@Column(name = "com_etab")
	private String comEtab;
	@Id
	@Column(name = "type_unite")
	private String typeUnite;
	@Id
	@Column(name = "ers")
	private String ers;
	@Id
	@Column(name = "nummca")
	private String nummca;
	
	public String getDepEtab() {
		return depEtab;
	}
	public void setDepEtab(String depEtab) {
		this.depEtab = depEtab;
	}
	public String getComEtab() {
		return comEtab;
	}
	public void setComEtab(String comEtab) {
		this.comEtab = comEtab;
	}
	public String getTypeUnite() {
		return typeUnite;
	}
	public void setTypeUnite(String typeUnite) {
		this.typeUnite = typeUnite;
	}
	public String getErs() {
		return ers;
	}
	public void setErs(String ers) {
		this.ers = ers;
	}
	public String getNummca() {
		return nummca;
	}
	public void setNummca(String nummca) {
		this.nummca = nummca;
	}

	
}
