package fr.insee.p7mcaspringboot.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class MCAMotPK implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7147950039610752043L;

	private String depEtab;
	private String comEtab;
	private String typeUnite;
	private String ers;
	private String nummca;
	
	public MCAMotPK() {
	} 
	
	public MCAMotPK(String depEtab, String comEtab, String typeUnite, String ers, String nummca) {
		super();
		this.depEtab = depEtab;
		this.comEtab = comEtab;
		this.typeUnite = typeUnite;
		this.ers = ers;
		this.nummca = nummca;
	}
	
	
	
	
	public String getDepEtab() {
		return depEtab;
	}
	public void setDepEtab(String depEtab) {
		this.depEtab = depEtab;
	}
	public String getComEtab() {
		return comEtab;
	}
	public void setComEtab(String comEtab) {
		this.comEtab = comEtab;
	}
	public String getTypeUnite() {
		return typeUnite;
	}
	public void setTypeUnite(String typeUnite) {
		this.typeUnite = typeUnite;
	}
	public String getErs() {
		return ers;
	}
	public void setErs(String ers) {
		this.ers = ers;
	}
	public String getNummca() {
		return nummca;
	}
	public void setNummca(String nummca) {
		this.nummca = nummca;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comEtab == null) ? 0 : comEtab.hashCode());
		result = prime * result + ((depEtab == null) ? 0 : depEtab.hashCode());
		result = prime * result + ((ers == null) ? 0 : ers.hashCode());
		result = prime * result + ((nummca == null) ? 0 : nummca.hashCode());
		result = prime * result + ((typeUnite == null) ? 0 : typeUnite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MCAMotPK other = (MCAMotPK) obj;
		if (comEtab == null) {
			if (other.comEtab != null)
				return false;
		} else if (!comEtab.equals(other.comEtab))
			return false;
		if (depEtab == null) {
			if (other.depEtab != null)
				return false;
		} else if (!depEtab.equals(other.depEtab))
			return false;
		if (ers == null) {
			if (other.ers != null)
				return false;
		} else if (!ers.equals(other.ers))
			return false;
		if (nummca == null) {
			if (other.nummca != null)
				return false;
		} else if (!nummca.equals(other.nummca))
			return false;
		if (typeUnite == null) {
			if (other.typeUnite != null)
				return false;
		} else if (!typeUnite.equals(other.typeUnite))
			return false;
		return true;
	}
}