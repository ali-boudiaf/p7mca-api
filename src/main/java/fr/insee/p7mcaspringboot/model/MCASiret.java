package fr.insee.p7mcaspringboot.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.util.comparator.Comparators;

@Entity
@Table(name = "mca_sirets")
public class MCASiret implements Comparable<MCASiret> {

	@Id
	@Column(name = "nummca")
	private String nummca;

	private String siren;
	@Column(name = "cj_cp")
	private String cjCp;
	@Column(name = "denom_ou_nom_condense")
	private String denomOuNomCondense;
	private String sigle;
	private String apen;
	@Column(name = "eff3112_tr")
	private String eff3112Tr;
	@Column(name = "nic_siege")
	private String nicSiege;
	@Column(name = "depcom_siege")
	private String depcomSiege;
	private String siret;
	@Column(name = "enseigne_et1")
	private String enseigneEt1;
	@Column(name = "etat_et")
	private String etatEt;
	@Column(name = "expl_et")
	private String explEt;
	private String apet;
	@Column(name = "empl_et")
	private String emplEt;
	@Column(name = "eff3112_tr_et")
	private String eff3112TrEt;
	@Column(name = "dep_etab")
	private String depEtab;
	@Column(name = "com_etab")
	private String comEtab;
	@Column(name = "adr_et_com_lib")
	private String adrEtComLib;
	@Column(name = "adr_et_voie_lib")
	private String adrEtVoieLib;
	@Column(name = "adr_et_voie_num")
	private String adrEtVoieNum;
	@Column(name = "adr_et_voie_repet")
	private String adrEtVoieRepet;
	@Column(name = "adr_et_voie_type")
	private String adrEtVoieType;
	@Column(name = "adr_et_rivoli")
	private String adrEtRivoli;
	@Column(name = "type_unite")
	private String typeUnite;
	private String mot1;
	private String mot2;
	private String mot3;
	private String mot4;
	private String mot5;
	private String mot6;
	private String mot7;
	@Column(name = "origine_ers")
	private String origineErs;
	private String source;
	private String dermot;
	@Transient
	private int noteClasse = 100;
	@Transient
	private int noteNom = 0;
	@Transient
	private int noteAdresse = 100;

	@Override
	public String toString() {
		return "MCASiret [noteClasse=" + noteClasse + ", noteNom=" + noteNom + ", noteAdresse=" + noteAdresse + "]";
	}

	public String getNummca() {
		return nummca;
	}

	public void setNummca(String nummca) {
		this.nummca = nummca;
	}

	public String getSiren() {
		return siren;
	}

	public void setSiren(String siren) {
		this.siren = siren;
	}

	public String getCjCp() {
		return cjCp;
	}

	public void setCjCp(String cjCp) {
		this.cjCp = cjCp;
	}

	public String getDenomOuNomCondense() {
		return denomOuNomCondense;
	}

	public void setDenomOuNomCondense(String denomOuNomCondense) {
		this.denomOuNomCondense = denomOuNomCondense;
	}

	public String getSigle() {
		return sigle;
	}

	public void setSigle(String sigle) {
		this.sigle = sigle;
	}

	public String getApen() {
		return apen;
	}

	public void setApen(String apen) {
		this.apen = apen;
	}

	public String getEff3112Tr() {
		return eff3112Tr;
	}

	public void setEff3112Tr(String eff3112Tr) {
		this.eff3112Tr = eff3112Tr;
	}

	public String getNicSiege() {
		return nicSiege;
	}

	public void setNicSiege(String nicSiege) {
		this.nicSiege = nicSiege;
	}

	public String getDepcomSiege() {
		return depcomSiege;
	}

	public void setDepcomSiege(String depcomSiege) {
		this.depcomSiege = depcomSiege;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public String getEnseigneEt1() {
		return enseigneEt1;
	}

	public void setEnseigneEt1(String enseigneEt1) {
		this.enseigneEt1 = enseigneEt1;
	}

	public String getEtatEt() {
		return etatEt;
	}

	public void setEtatEt(String etatEt) {
		this.etatEt = etatEt;
	}

	public String getExplEt() {
		return explEt;
	}

	public void setExplEt(String explEt) {
		this.explEt = explEt;
	}

	public String getApet() {
		return apet;
	}

	public void setApet(String apet) {
		this.apet = apet;
	}

	public String getEmplEt() {
		return emplEt;
	}

	public void setEmplEt(String emplEt) {
		this.emplEt = emplEt;
	}

	public String getEff3112TrEt() {
		return eff3112TrEt;
	}

	public void setEff3112TrEt(String eff3112TrEt) {
		this.eff3112TrEt = eff3112TrEt;
	}

	public String getDepEtab() {
		return depEtab;
	}

	public void setDepEtab(String depEtab) {
		this.depEtab = depEtab;
	}

	public String getComEtab() {
		return comEtab;
	}

	public void setComEtab(String comEtab) {
		this.comEtab = comEtab;
	}

	public String getAdrEtComLib() {
		return adrEtComLib;
	}

	public void setAdrEtComLib(String adrEtComLib) {
		this.adrEtComLib = adrEtComLib;
	}

	public String getAdrEtVoieLib() {
		return adrEtVoieLib;
	}

	public void setAdrEtVoieLib(String adrEtVoieLib) {
		this.adrEtVoieLib = adrEtVoieLib;
	}

	public String getAdrEtVoieNum() {
		return adrEtVoieNum;
	}

	public void setAdrEtVoieNum(String adrEtVoieNum) {
		this.adrEtVoieNum = adrEtVoieNum;
	}

	public String getAdrEtVoieRepet() {
		return adrEtVoieRepet;
	}

	public void setAdrEtVoieRepet(String adrEtVoieRepet) {
		this.adrEtVoieRepet = adrEtVoieRepet;
	}

	public String getAdrEtVoieType() {
		return adrEtVoieType;
	}

	public void setAdrEtVoieType(String adrEtVoieType) {
		this.adrEtVoieType = adrEtVoieType;
	}

	public String getAdrEtRivoli() {
		return adrEtRivoli;
	}

	public void setAdrEtRivoli(String adrEtRivoli) {
		this.adrEtRivoli = adrEtRivoli;
	}

	public String getTypeUnite() {
		return typeUnite;
	}

	public void setTypeUnite(String typeUnite) {
		this.typeUnite = typeUnite;
	}

	public String getMot1() {
		return mot1;
	}

	public void setMot1(String mot1) {
		this.mot1 = mot1;
	}

	public String getMot2() {
		return mot2;
	}

	public void setMot2(String mot2) {
		this.mot2 = mot2;
	}

	public String getMot3() {
		return mot3;
	}

	public void setMot3(String mot3) {
		this.mot3 = mot3;
	}

	public String getMot4() {
		return mot4;
	}

	public void setMot4(String mot4) {
		this.mot4 = mot4;
	}

	public String getMot5() {
		return mot5;
	}

	public void setMot5(String mot5) {
		this.mot5 = mot5;
	}

	public String getMot6() {
		return mot6;
	}

	public void setMot6(String mot6) {
		this.mot6 = mot6;
	}

	public String getMot7() {
		return mot7;
	}

	public void setMot7(String mot7) {
		this.mot7 = mot7;
	}

	public String getOrigineErs() {
		return origineErs;
	}

	public void setOrigineErs(String origineErs) {
		this.origineErs = origineErs;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDermot() {
		return dermot;
	}

	public void setDermot(String dermot) {
		this.dermot = dermot;
	}

	public int getNoteNom() {
		return noteNom;
	}

	public void setNoteNom(int noteNom) {
		this.noteNom = noteNom;
	}

	public int getNoteAdresse() {
		return noteAdresse;
	}

	public void setNoteAdresse(int noteAdresse) {
		this.noteAdresse = noteAdresse;
	}

	public int getNoteClasse() {
		return noteClasse;
	}

	public void setNoteClasse(int noteClasse) {
		this.noteClasse = noteClasse;
	}

	@Override
	public int compareTo(MCASiret o) {
		return Comparator.comparingInt(MCASiret::getNoteClasse).thenComparingInt(MCASiret::getNoteAdresse)
				.thenComparing(MCASiret::getNoteNom, Comparator.reverseOrder()).compare(this, o);
	}

}
