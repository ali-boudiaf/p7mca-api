package fr.insee.p7mcaspringboot.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

public class MCASiretTest {

	@Test
	public final void MCASiretCompareTest() {
		// GIVEN
		MCASiret siretMeilleureClasse = new MCASiret();
		siretMeilleureClasse.setNoteClasse(1);
		siretMeilleureClasse.setNoteAdresse(10);
		siretMeilleureClasse.setNoteNom(1000);
		MCASiret siret1 = new MCASiret();
		siret1.setNoteClasse(3);
		siret1.setNoteAdresse(32);
		siret1.setNoteNom(0);
		MCASiret siret2 = new MCASiret();
		siret2.setNoteClasse(3);
		siret2.setNoteAdresse(32);
		siret2.setNoteNom(70);
		MCASiret siret3 = new MCASiret();
		siret3.setNoteClasse(3);
		siret3.setNoteAdresse(12);
		siret3.setNoteNom(70);

		MCASiret siretPasNotee = new MCASiret();
		MCASiret siretBadClasse = new MCASiret();
		siretBadClasse.setNoteClasse(9);
		siretBadClasse.setNoteAdresse(32);
		siretBadClasse.setNoteNom(0);

		List<MCASiret> mesMCASirets = new ArrayList<>();
		mesMCASirets.add(siret1);
		mesMCASirets.add(siret2);
		mesMCASirets.add(siret3);
		mesMCASirets.add(siretPasNotee);
		mesMCASirets.add(siretBadClasse);
		mesMCASirets.add(siretMeilleureClasse);

		// WHEN
		mesMCASirets = mesMCASirets.stream().sorted().collect(Collectors.toList());

		// THEN
		assertEquals(mesMCASirets.get(0), siretMeilleureClasse);
		assertEquals(mesMCASirets.get(1), siret3);
		assertEquals(mesMCASirets.get(2), siret2);
		assertEquals(mesMCASirets.get(3), siret1);
		assertEquals(mesMCASirets.get(4), siretBadClasse);
		assertEquals(mesMCASirets.get(5), siretPasNotee);
	}
}
